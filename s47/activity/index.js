//====== EVENT AND EVENT LISTENERS ========
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
const txtLastName = document.querySelector("#txt-last-name");

console.log(txtFirstName);
console.log(spanFullName);


// Activity

txtLastName.addEventListener("keyup", updateFullName);
txtFirstName.addEventListener("keyup", updateFullName);

function updateFullName(event) {
  spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}