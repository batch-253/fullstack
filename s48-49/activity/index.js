fetch('https://jsonplaceholder.typicode.com/posts')
    .then((response) => response.json())
    .then((data) => showPosts(data));


const showPosts = (posts) => {

    let postEntries = '';


    posts.forEach((post) => {
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button id="click-${post.id}" onClick="editPost('${post.id}')">Edit</button>
                <button id="delete-${post.id}" onClick="deletePost('${post.id}')">Delete</button
            </div>
        `;
    });


    console.log(postEntries);


    document.querySelector('#div-post-entries').innerHTML = postEntries;
};

const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: 'DELETE',
    })
        .then(() => {

            document.querySelector(`#post-title-${id}`).remove();
            document.querySelector(`#post-body-${id}`).remove();
            document.querySelector(`#click-${id}`).remove();
            document.querySelector(`#delete-${id}`).remove();

            console.log(`Post with ID ${id} deleted successfully.`);
        })
        .catch((error) => {
            console.error(`Error deleting post with ID ${id}: ${error}`);
        });
};